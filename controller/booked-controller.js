import Model from "../model/index-model.js";

class Controller {
    constructor () {
        this.model= new Model ()
    }
    async getBookingsByBookingID(booking_id) {
        return this.model.getBookingByBookingID(booking_id)
    }
    async getBookingsByPhone(phone) {
        return this.model.getBookingsByPhone(phone)
    }
    async getSeatsByBookingID(booking_id) {
        return this.model.getSeatsByBookingID(booking_id)
    }
    async getScreeningAndScreen(show_id) {
        return this.model.getScreeningAndScreen(show_id)
    }
}
export default Controller