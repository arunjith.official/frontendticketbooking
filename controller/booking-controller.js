import Model from "../model/index-model.js";
class Controller {
    constructor() {
        this.model=new Model ()
    }
    async getLastBooking(){
        return this.model.getLastBooking()
    }
    async createBooking(booked_by,phone,show_id) {
        this.model.createBooking(booked_by,phone,show_id)
    }
    async createSeatsBooked(selectedSeats,booking_id,show_id) {
        return this.model.createSeatsBooked(selectedSeats,booking_id,show_id)
    }
}

export default Controller