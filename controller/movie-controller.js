import Model from "../model/index-model.js";

class Controller {
    constructor() {
        this.model= new Model ()
    }
    async getMovieByID(movie_id){
        return this.model.getMovieByID(movie_id)
    }
    async getScreeningByMovieID (movie_id) {
        return this.model.getScreeningByMovieID(movie_id)
    }
    
}

export default Controller