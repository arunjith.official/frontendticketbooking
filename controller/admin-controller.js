import Model from "../model/index-model.js"
class Controller {
    constructor () {
        this.model=new Model ()
    }
    async createMovie(movie_name,movie_duration,release_date,movie_description) {
        console.log(movie_name,release_date)
        return this.model.createMovie(movie_name,movie_duration,release_date,movie_description)
    }
    async createScreen(screen_name,total_seats,screen_description) {
        return this.model.createScreen(screen_name,total_seats,screen_description) 
    }
    async createScreening(movie_id,screen_id,show_time) {
        return this.model.createScreening(movie_id,screen_id,show_time) 
    }
    async getBookings(){
        return this.model.getBookings()
    }
    async getUsers() {
        return this.model.getUsers()
    }
}

export default Controller