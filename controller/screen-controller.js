import Model from "../model/index-model.js";

class Controller {
    constructor() {
        this.model=new Model ()
    }
    async getSeats () {
        return this.model.getSeats()
    }
    async getSeatsBookedByShowID (show_id) {
        return this.model.getSeatsBookedByShowID(show_id)
    }
    async getScreeningAndScreen (show_id) {
        return this.model.getScreeningAndScreen(show_id)
    }
    async getBookedSeats (show_id) {
        return this.model.getBookedSeats(show_id)
    }
    async loadSeats (show_id) {
        return this.model.loadSeats(show_id) 
    } 
    async avilabilityCheck (i) {
        return this.model.avilabilityCheck(i)
    }
    getSeatsStatus (){
        return this.model.getSeatsStatus()
    }
    selectSeats (seat_id) {
        return this.model.selectSeats(seat_id)
    }
    renderSeats () {
        return this.model.renderSeats()
    }
}

export default Controller