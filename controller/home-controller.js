import Model from "../model/index-model.js";

class Controller {
    constructor () {
        this.model=new Model ()
    }
    getMovies () {
        return this.model.getMovies()
    }
}

export default Controller