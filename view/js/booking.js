import Controller from "../controller/booking-controller.js";

const controller = new Controller();

const bookingDiv = document.getElementById("main");
const movieName = document.getElementById("movieName");
const screenID = document.getElementById("screenID");
const seatsSelected = document.getElementById("seatsSelected");


const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));
console.log(selectedSeats)
const movieAndScreen = JSON.parse(localStorage.getItem('movieAndScreen'))
console.log(movieAndScreen)
const showID = JSON.parse(localStorage.getItem('showID'));
console.log(showID)


renderForm(selectedSeats, movieAndScreen)


function renderForm(seats, info) {
    movieName.innerHTML = `
    <span>${info[1]}</span>`
    screenID.innerHTML = `
    <span>${info[0]}</span>`
    seatsSelected.innerHTML = `
    <span>The seats selected 
    ${seats.filter(element =>
        typeof element === 'number')
        }</span>`

}

const phone = document.getElementById("phone");
const form = document.getElementById("form");
const errorElementName = document.getElementById("nameError");
const name = document.getElementById("name");
const button = document.getElementById("button")

form.addEventListener("submit", (e) => {
    e.preventDefault()
    book(phone.value, name.value, showID)
    console.log(phone.value)
    console.log(name.value)
})


function book(phone, name, showID) {
    localStorage.setItem('phone', phone)
    controller.createBooking(name, phone, showID).then((rr) => {
        controller.getLastBooking().then((data) =>
            bookseats(data)
        )
    })
}


function bookseats(data) {
    let booking_id = data
    console.log(data)
    console.log(selectedSeats)
    localStorage.setItem('booking_id', data)
    controller.createSeatsBooked(selectedSeats, data, showID).then((response) => {
        localStorage.setItem('bookedSeats', JSON.stringify(selectedSeats))
        localStorage.setItem('booking_id', JSON.stringify(booking_id))
    }).then(r => {
        window.location.href = `http://127.0.0.1:5500/view/booked.html?id=${booking_id}`
    })
}


