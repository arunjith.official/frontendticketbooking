import Controller from "../controller/movie-controller.js";

const controller = new Controller();
const screenDiv = document.getElementById('screens_main');
const titleDiv =document.getElementById('title')


const queryString =window.location.search
console.log(queryString)
const urlSearchParams = new URLSearchParams(queryString);
console.log(urlSearchParams)
const movie_id = urlSearchParams.get('movie_id')
console.log(movie_id)


controller.getMovieByID(movie_id).then((data) => {
    renderMovie(data)
    console.log(data)
})
controller.getScreeningByMovieID(movie_id).then((data) => {
    console.log(data)
    renderScreening(data)
})

.catch ( (error) => {
    console.log (error)
});

function renderMovie (movie) {
    console.log(movie)
    const boxDiv = document.createElement('div')
    boxDiv.classList.add('movie_details')
    boxDiv.innerHTML=`
    <h2 class="movieName">${movie.movie_name}</h2>
    <p class="movieDescription">${movie.movie_description}</p>
    <h2 class="sub">This movie is currently running on</h2>
    `
    titleDiv.append(boxDiv)

}


function renderScreening (screening) {
    console.log(screening)
    screening.forEach((screens,i) =>  {
        const date =new Date(screens.show_time)
        console.log(date)
        const options = { weekday: 'long', hour:'numeric',minute:'numeric' };
        const Time = date.toLocaleDateString('en-us',options)
        const boxDiv = document.createElement('div')
        boxDiv.classList.add('screens')
        boxDiv.innerHTML=`
        <span class="screen">
        Screen 
        ${screens.screen_id.screen_name}
        </span>
        <p class="screen"> ${Time}</p>
        <button onclick="window.location.href='http://127.0.0.1:5500/view/screen.html?show_id=${screens._id}';">Book seats</button>`
        
        screenDiv.append(boxDiv)
    });   
}

