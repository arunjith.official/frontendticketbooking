import Controller from "../controller/screen-controller.js";

const controller = new Controller();
const seatsDiv = document.getElementById("seats");
const infoDiv = document.getElementById("info")
const buttonDiv = document.getElementById("buttonProceed")


const queryString = window.location.search
const urlSearchParams = new URLSearchParams(queryString)
const show_id = urlSearchParams.get('show_id')
console.log(show_id)

var totalSeats = controller.getScreeningAndScreen(show_id).then((data) => {
    console.log(data.screening[0].screen_id.total_seats)
    totalSeats = (data.screening[0].screen_id.total_seats)
    console.log(data.screening[0])
    renderInfo(data.screening[0].movie_id, data.screening[0].screen_id)
    return totalSeats
})

    // controller.getScreeningAndScreen(show_id).then((data) => {
    //     console.log(data)
    //     renderInfo(data)
    // })

    .catch((error) => {
        console.log(error)
    });



var bookedSeats = controller.getSeatsBookedByShowID(show_id).then((data) => {
    console.log(data)
    const array = data.seats_booked
    const bookedSeats = array.map(({ seat_id }) => (seat_id))
    console.log(bookedSeats)
    return bookedSeats
})
    .catch((error) => {
        console.log(error)
    });

function renderInfo(movie, screen) {
    console.log(movie, screen)
    const movie_name = movie.movie_name
    const screen_name = screen.screen_name
    const nameDiv = document.createElement("movie_info")
    nameDiv.classList.add("movie_name")
    nameDiv.innerHTML = `
    <h3 class="movie_name">${movie_name}</h3>
    <h4 class="screen_name">${screen_name}</h4>
    <h4 class="seats_title">Seats</h4>`

    const movieAndScreen = []
    movieAndScreen.push(movie_name)
    movieAndScreen.push(screen_name)
    console.log(movieAndScreen)
    localStorage.setItem('movieAndScreen', JSON.stringify(movieAndScreen))
    infoDiv.append(nameDiv)
}



controller.loadSeats(show_id).then((data) => {
    console.log(data)
    renderSeats(controller.renderSeats())
})




function selectSeat(i) {
    controller.selectSeats(i)
    renderSeats(controller.renderSeats())
}
function renderSeats(data) {
    let selectedSeats = []
    totalSeats = data.length
    seatsDiv.innerHTML = ``
    for (let i = 0; i < totalSeats; i++) {
        const boxDiv = document.createElement('div')
        boxDiv.classList.add('seat')
        if (data[i] === "booked") {
            boxDiv.innerHTML = `
                <div class="booking_booked">
                <div class="seat_no">
                <span>${i + 1}</span>
                </div>
                </div>`
            seatsDiv.append(boxDiv)
        }
        else if (data[i] === "selected") {
            selectedSeats.push(i + 1)
            boxDiv.innerHTML = `
                <div class="seat_div">
                <div class="booking_selected">
                <div class="seat_no">
                <span>${i + 1}</span>
                </div>
                </div>
                </div>`
            seatsDiv.append(boxDiv)
        }
        else if (data[i] === "available") {
            boxDiv.innerHTML = `
                <div class="seat_div">
                <div class="booking_unbooked" data-seatNo="${i}">
                <div class="seat_no">
                <span>${i + 1}</span>
                </div>
                </div>
                </div>`
            seatsDiv.append(boxDiv)
        }
    }

    const newlyAddedSeats = document.getElementsByClassName('seat')
    for (let i = 0; i < newlyAddedSeats.length; i++) {
        newlyAddedSeats[i].addEventListener('click', () => {
            console.log(i)
            selectSeat(i)
        })
    }
    const seatsArray = controller.getSeatsStatus()

    for (let i = 0; i < selectedSeats.length; i++) {
        if (seatsArray[selectedSeats[i] - 1] === "available") {
            delete selectedSeats[i]
        }

        console.log(show_id)
        localStorage.setItem('selectedSeats', JSON.stringify(selectedSeats));
        localStorage.setItem('showID', JSON.stringify(show_id));
        renderButton()
    }
}



function renderButton() {
    buttonDiv.innerHTML = `<button class="seats_proceed" 
    onclick="window.location.href='./booking.html'">
    Proceed</button>`
}