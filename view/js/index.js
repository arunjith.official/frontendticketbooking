import Controller from '../controller/home-controller.js';

const controller = new Controller();
const moviesDiv = document.getElementById('movies')

controller.getMovies().then((data) => {
    console.log(data)
    renderMovies(data)
})
.catch ( (error) => {
    console.log (error)
} );

function renderMovies(movies){
    console.log(movies)
    movies.forEach((movie, i) => {
        const boxDiv = document.createElement('div')
        boxDiv.classList.add('box')
        boxDiv.innerHTML=`
        <span>${movie.movie_name}</span>
        <small>${movie.release_date}</small>
        <p class="movie_description">${movie.movie_description}</p>
        <button class="box_button" 
        onclick="window.location.href='http://127.0.0.1:5500/view/movie.html?movie_id=${movie._id}';">
          Book the movie
        </button>
        `
        moviesDiv.append(boxDiv)
    });
}

