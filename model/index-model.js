class Model {
    constructor() {
        this.totalSeats;
        this.seatsArray = [];
    }
    async getSeats() {
        const response = await fetch('http://localhost:3000/seats')
        const seats = await response.json()
        // console.log(seats)
        return seats.seats
    }
    async getMovies() {
        const response = await fetch('http://localhost:3000/movie')
        const movies = await response.json()
        console.log(movies)
        return movies.movies
    }
    async getMovieByID(movie_id) {
        const response = await fetch(`http://localhost:3000/movie/${movie_id}`)
        const movie = await response.json()
        console.log(movie[0])
        return movie[0]
    }
    async getScreeningByMovieID(movie_id) {
        const response = await fetch(`http://localhost:3000/screening/movie/${movie_id}`)
        const screenings = await response.json()
        console.log(screenings)
        return screenings.screening
    }
    async getSeatsBookedByShowID(show_id) {
        const response = await fetch(`http://localhost:3000/seats-booked/${show_id}`)
        const seats_booked = await response.json()
        console.log(seats_booked)
        return seats_booked
    }
    async getScreeningAndScreen(show_id) {
        const response = await fetch(`http://localhost:3000/screening/show/${show_id}`)
        const screens = await response.json()
        return screens
    }
    async getTotalSeats(show_id) {
        const response = await fetch(`http://localhost:3000/screening/show/${show_id}`)
        const data = await response.json()
        this.totalSeats = (data.screening[0].screen_id.total_seats)
        return this.totalSeats
    }
    async getBookedSeats(show_id) {
        const response = await fetch(`http://localhost:3000/seats-booked/${show_id}`)
        const data = await response.json()
        const seatsBooked = data.seats_booked
        const seats = seatsBooked.map(({ seat_id }) => (seat_id))
        return seats
    }

    async loadSeats(show_id) {
        let totalSeats = await this.getTotalSeats(show_id)
        let seatsBooked = await this.getBookedSeats(show_id)
        let seatsArray = []
        for (let i = 0; i < totalSeats; i++) {
            if (seatsBooked.includes((i + 1))) {
                seatsArray.push("booked")
            }
            else {
                seatsArray.push("available")
            }
        }
        this.seatsArray = seatsArray
        console.log(this.seatsArray)
        return this.seatsArray
    }
    renderSeats() {
        
        return this.seatsArray
    }
    selectSeats(seat_id) {
        if (this.seatsArray[seat_id] === "available") {
            this.seatsArray[seat_id] = "selected"          
        }
        else if (this.seatsArray[seat_id] === "selected") {
            this.seatsArray[seat_id] = "available"
        }
        
    }
    avilabilityCheck(i) {
        if (this.seatsArray[i] === "available") {
            this.seatsArray[i] = "selected"
        }
        else if (this.seatsArray[i] === "selected") {
            this.seatsArray[i] = "available"
        }
        console.log(this.seatsArray)
        return this.seatsArray
    }
    //for booking.html
    async getLastBooking() {
        const response = await fetch(`http://localhost:3000/book`)
        const data = await response.json()
        console.log(data)
        const lastData = data.bookings[data.bookings.length - 1]
        console.log(lastData)
        const lastBookingID = lastData._id
        console.log(lastBookingID)
        return lastBookingID 
    }
    async createSeatsBooked(selectedSeats, booking_id, show_id) {
        for (let i = 0; i < selectedSeats.length; i++) {
            let seat = selectedSeats[i]
            const seatData = {
                seat_id: seat,
                booking_id: booking_id,
                show_id: show_id
            };
            const jsonData = JSON.stringify(seatData)
            console.log(jsonData)
            console.log(seatData)
            await fetch(`http://localhost:3000/seats-booked`,
                {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                        // 'Accept' : 'json',
                        'Content-Type': 'application/json',
                    },
                    body: jsonData
                })
                .then((result) => {
                    console.log(result)
                    console.log("created SeatsBooked successfully")
                   
                })
                .catch((error) => {
                    
                })
        }
    }
    async createBooking(booked_by, phone, show_id) {
        const data = {
            booked_by: booked_by,
            phone: phone,
            show_id: show_id
        };
        const jsonData = JSON.stringify(data)
        console.log(jsonData)
        console.log(data)
        await fetch("http://localhost:3000/book",
            {
                method: 'POST',
                // mode: 'no-cors' ,
                headers: {
                    // 'Accept' : 'json',
                    'Content-Type': 'application/json'
                },
                body: jsonData
            })
            .then((response) => {
                console.log(response)
                return true
            })
            .catch(err => {
                console.log(err)
                return false
            })
    }
    getSeatsStatus() {
        return this.seatsArray
    }
    async bookSeatsbyBookingid(seat_id, booking_id, show_id) {
        const response = await fetch(`http://localhost:3000/seats-booked`, {
            method: "post",
            headers: {
                'Accept': 'json',
                'Content-Type': 'json'
            },
            body: JSON.stringify({
                "seat_id": seat_id,
                "booking_id": booking_id,
                "show_id": show_id
            })
        })
            .then((response))
        console.log("seats booked successfully")
    }
    async getBookingByBookingID(booking_id) {
        const response = await fetch(`http://localhost:3000/book/${booking_id}`)
        const data = await response.json()
        return data
    }

    async getBookingsByPhone(phone) {
        const response = await fetch(`http://localhost:3000/book/phone/${phone}`)
        const data = await response.json()
        return data
    }

    async getSeatsByBookingID(booking_id) {
        const response = await fetch(`http://localhost:3000/seats-booked/booking/${booking_id}`)
        const data = await response.json()
        return data
    }

    async createScreen(screen_name, total_seats, screen_description) {
        console.log(screen_name, total_seats, screen_description)
        const response = await fetch(`http://localhost:3000/screen`, {
            method: "post",
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "screen_name": screen_name,
                "total_seats": total_seats,
                "description": screen_description
            })
        })
        console.log("screen created successfully")
    }


    async createMovie(movie_name, movie_duration, release_date, movie_description) {
        console.log(movie_name, movie_duration, release_date, movie_description)
        const response = await fetch(`http://localhost:3000/movie`, {
            method: "post",
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "movie_name": movie_name,
                "movie_duration": movie_duration,
                "release_date": release_date,
                "movie_description": movie_description
            })
        })
    }

    async createScreening(movie_id, screen_id, show_time) {
        const response = await fetch(`http://localhost:3000/screening`, {
            method: "post",
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "movie_id": movie_id,
                "screen_id": screen_id,
                "show_time": show_time
            })
        })
        console.log("screening created successfully")
    }

    //for admin
    async getBookings() {
        const response = await fetch(`http://localhost:3000/book`)
        const data = await response.json()
        const bookings = data.bookings
        console.log(bookings)
        return bookings
    }

    async getUsers() {
        const response = await fetch(`http://localhost:3000/user`)
        const data = await response.json()
        const users = data.users
        console.log(users)
        return users
    }

    async createUser(username, password) {
        const response = await fetch(`http://localhost:3000/user`, {
            method: "post",
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": username,
                "password": password
            })
        })
        console.log("screening created successfully")
    }
}

export default Model